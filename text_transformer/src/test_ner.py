#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 17:13:36 2019

@author: didelani
"""
import os
import pandas as pd
import numpy as np
import argparse
import sys
from flair.data import Sentence
from flair.models import SequenceTagger
from flair.data import Corpus
from flair.datasets import ColumnCorpus
from flair.embeddings import TokenEmbeddings, WordEmbeddings, StackedEmbeddings
from typing import List
from flair.trainers import ModelTrainer
from flair.visual.training_curves import Plotter

model = SequenceTagger.load('../notebooks/resources/taggers/uncased-ner/best-model.pt')

def predict_ner_output_sentence_per_line(data_dir, textfile):    
    with open(data_dir + textfile) as f:
        file_lines = f.readlines()
        
    tagged_sentences = []
    for line in file_lines:
        sentr = line.strip()
        
        sentence = Sentence(sentr)

        # predict tags and print
        model.predict(sentence)
        tagged_sent = sentence.to_tagged_string()
        tagged_sentences.append(tagged_sent)
        
    coNLL_format_tags = []
    
    for tagged_sent in tagged_sentences:
        tagged_words = tagged_sent.split()
        for k, tagged_word in enumerate(tagged_words):
            token = tagged_word
            if token.startswith('<') and token.endswith('>'):
                continue
            
            if k < len(tagged_words)-1:
                next_token = tagged_words[k+1]
            else:
                next_token = ''
            
            if next_token.startswith('<') and next_token.endswith('>'):
                tag = next_token[1:-1]
            else:
                tag = 'O'
            coNLL_format_tags.append([token, tag])
        coNLL_format_tags.append(['', ''])
        
    return coNLL_format_tags
    
if __name__ == '__main__':
    
    data_dir = '../data/ground_truth/'
    output_dir = '../data/ground_truth_ref_ner_output/'
    train_file = 'train.sp.ref'
    dev_file = 'dev.sp.ref'
    test_file = 'test.sp.ref'
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    train_res = predict_ner_output_sentence_per_line(data_dir, train_file)
    train_df = pd.DataFrame(train_res)
    train_df.to_csv(output_dir+train_file+'_ner.tsv', sep='\t', header=False, index=False)
    
    
    dev_res = predict_ner_output_sentence_per_line(data_dir, dev_file)
    dev_df = pd.DataFrame(dev_res)
    dev_df.to_csv(output_dir+dev_file+'_ner.tsv', sep='\t', header=False, index=False)
    
    test_res = predict_ner_output_sentence_per_line(data_dir, test_file)
    test_df = pd.DataFrame(test_res)
    test_df.to_csv(output_dir+test_file+'_ner.tsv', sep='\t', header=False, index=False)
