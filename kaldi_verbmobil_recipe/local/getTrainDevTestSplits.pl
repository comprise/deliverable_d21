#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <trascriptions> <outdir>\n";
	exit(0);
}

my $probSpkrsA = " CAC JDH NKH RGM ";	# speaker ids representing different speakers in VM1 and VM2; keep the begin n end spaces
my $probSpkrsB = "MAS";					# speaker id  representing different speakers in VM1

my $test2devSpkrs = " HCFVMX QZOVMX QZBVMX ";		# move these speakers from test to dev to create a balance 

my $accentedSpkrTest  = "ADBVMX";	# Bristish accented 
my $accentedSpkrDev   = "VNCVMX";   # Asian accented

my %spkrIn;
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my ($vmid, $dialogid, $turnid, $spkrid, $cdversion, $cdid) = split(/\_/, $id);
	
	if($probSpkrsA =~ m/ $spkrid /){
		$spkrid = $spkrid.$vmid; # append VM id coz speaker id represents different speakers in VM1 and VM2		
	}elsif(($spkrid eq $probSpkrsB) && ($dialogid =~ m/q007n/i)){
		$spkrid = $spkrid."VMZ"; # to keep speaker ids consistent
	}else{
		$spkrid = $spkrid."VMX"; # to keep speaker ids consistent
	}

	if(($test2devSpkrs =~ m/ $spkrid /) || ($spkrid eq $accentedSpkrDev)){
		$spkrIn{$spkrid} = "dev";		# preference to dev
	}elsif(($id =~ m/\_(47|52|55|56)$/) || ($id =~ m/VM1\_q0(1\d|20)/) || ($spkrid eq $accentedSpkrTest)){	# test set includes VM52, VM55, VM 56, q010-q020 in VM6,
		$spkrIn{$spkrid} = "test";
	}elsif(($id =~ m/\_(32|51)$/) || ($id =~ m/VM1\_q0(0\d|21|22)/)){	# dev  set includes VM32, VM51, q001-q009 q021 q022 in VM6,  
		if($spkrIn{$spkrid} ne "test"){			# preference to test
			$spkrIn{$spkrid} = "dev";
		}
	}else{
		if(($spkrIn{$spkrid} ne "test") && ($spkrIn{$spkrid} ne "dev")){
			$spkrIn{$spkrid} = "train";			# preference to test and dev
		}
	}
}
close($FH);

my %dialogIn;
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my ($vmid, $dialogid, $turnid, $spkrid, $cdversion, $cdid) = split(/\_/, $id);

	if($probSpkrsA =~ m/ $spkrid /){
		$spkrid = $spkrid.$vmid; # append VM id coz speaker id represents different speakers in VM1 and VM2		
	}elsif(($spkrid eq $probSpkrsB) && ($dialogid =~ m/q007n/i)){
		$spkrid = $spkrid."VMZ"; # to keep speaker ids consistent
	}else{
		$spkrid = $spkrid."VMX"; # to keep speaker ids consistent
	}

	$dialogid =~ s/\d$//;		
	if($spkrIn{$spkrid} eq "test"){
		$dialogIn{$dialogid} = "test";
	}elsif($spkrIn{$spkrid} eq "dev"){
		if($dialogIn{$dialogid} ne "test"){
			$dialogIn{$dialogid} = "dev";			
		}
	}else{
		if(($dialogIn{$dialogid} ne "test") && ($dialogIn{$dialogid} ne "dev")){
			$dialogIn{$dialogid} = "train";		
		}	
	}
}
close($FH);

my $outdir = $ARGV[1];
$outdir =~ s/\/$//;
my $trainfile = "$outdir/train.transcriptions";
my $devfile = "$outdir/dev.transcriptions";
my $testfile = "$outdir/test.transcriptions";
open(TRAIN, '>', $trainfile) or die $!;
open(DEV, '>', $devfile) or die $!;
open(TEST, '>', $testfile) or die $!;
open my $FH2, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH2>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my ($vmid, $dialogid, $turnid, $spkrid, $cdversion, $cdid) = split(/\_/, $id);
	$dialogid =~ s/\d$//;		

	if($probSpkrsA =~ m/ $spkrid /){
		$spkrid = $spkrid.$vmid; # append VM id coz speaker id represents different speakers in VM1 and VM2		
	}elsif(($spkrid eq $probSpkrsB) && ($dialogid =~ m/q007n/i)){
		$spkrid = $spkrid."VMZ"; # to keep speaker ids consistent
	}else{
		$spkrid = $spkrid."VMX"; # to keep speaker ids consistent
	}

	if(($test2devSpkrs =~ m/ $spkrid /) || ($spkrid eq $accentedSpkrDev)){
		print DEV $line,"\n";		# preference to dev
	}elsif(($id =~ m/\_(47|52|55|56)$/) || ($id =~ m/VM1\_q0(1\d|20)/) ||($spkrid eq $accentedSpkrTest)){		# test set includes VM52, VM55, VM 56, q010-q020 in VM6,
		if(($spkrIn{$spkrid} eq "test") && ($dialogIn{$dialogid} eq "test")){
			print TEST $line,"\n";			
		}elsif(($spkrIn{$spkrid} eq "test") && ($dialogIn{$dialogid} eq "dev")){	# preference to test
			print TEST $line,"\n";			
		}else{
			print STDERR "Warning: Test set contamination for $id: SPKR:$spkrIn{$spkrid} DIAL:$dialogIn{$dialogid}\n";
		}
	}elsif(($id =~ m/\_(32|51)$/) || ($id =~ m/VM1\_q0(0\d|21|22)/)){	# dev  set includes VM32, VM51, q001-q009 q021 q022 in VM6,  
		if(($spkrIn{$spkrid} eq "dev") && ($dialogIn{$dialogid} eq "dev")){
			print DEV $line,"\n";			
		}elsif(($spkrIn{$spkrid} eq "test") && ($dialogIn{$dialogid} eq "test")){
			print TEST $line,"\n";			
		}elsif(($spkrIn{$spkrid} eq "dev") && ($dialogIn{$dialogid} eq "test")){
			print TEST $line,"\n";			# preference to test
		}else{
			print STDERR "Warning: Dev set contamination for $id: SPKR:$spkrIn{$spkrid} DIAL:$dialogIn{$dialogid}\n";
		}
	}else{
		if(($spkrIn{$spkrid} eq "train") && ($dialogIn{$dialogid} eq "train")){
			print TRAIN $line,"\n";			
		}else{
			if(($spkrIn{$spkrid} eq "dev") && ($dialogIn{$dialogid} eq "dev")){
				print DEV $line,"\n";			
			}elsif(($spkrIn{$spkrid} eq "train") && ($dialogIn{$dialogid} eq "dev")){
				print DEV $line,"\n";			# preference to dev
			}elsif(($spkrIn{$spkrid} eq "train") && ($dialogIn{$dialogid} eq "test")){
				print TEST $line,"\n";			# preference to dev
			}else{
				print STDERR "Warning: Train set contamination for $id: SPKR:$spkrIn{$spkrid} DIAL:$dialogIn{$dialogid}\n";
			}
		}
	}
}
close($FH2);
close(TRAIN);
close(DEV);
close(TEST);

