if($#ARGV != 1){
	print STDERR "USAGE: $0 <tmp_ctm_file> <out_ctm_dir>";
	exit(0);
}

my $lastid = "";
my $wrdctm = "";
my $fopenflag = 0;
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\r*\n+//;
	my ($fileid, $ch, $beg, $dur, $phn) = split(/\s+/, $line);

	if(($fileid ne $lastid) && $fopenflag){
		my $wctmfile = $ARGV[1]."/".$lastid.".ctm";
		open(my $FOUT, '>', $wctmfile) or die "can't read open '$wctmfile': $OS_ERROR";
		print $FOUT $wrdctm;
		close $FOUT;
		$wrdctm = "";
	}
	$wrdctm = $wrdctm.$line."\n";
	$fopenflag = 1;
	$lastid = $fileid;
}
close $FH or die "can't read close '$fp': $OS_ERROR";

my $wctmfile = $ARGV[1]."/".$lastid.".ctm";
open(my $FOUT, '>', $wctmfile) or die "can't read open '$wctmfile': $OS_ERROR";
print $FOUT $wrdctm;
close $FOUT;