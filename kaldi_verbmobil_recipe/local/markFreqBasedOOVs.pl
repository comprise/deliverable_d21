#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <orig_text_trascriptions> <cmudict>\n";
	exit(0);
}

my $WFTHRESHOLD = 1;
my $LEXWFTHRESHOLD = 1;

my %lexicon;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	$line = lc($line);
	my ($wrd, $lex) = split(/\s+/, $line, 2);
	$wrd =~ s/\(\d+\)$//;
	$lex =~ s/\d//g;
	if(exists($lexicon{$wrd})){
		$lexicon{$wrd} = $lexicon{$wrd}."|".$lex;
	}else{
		$lexicon{$wrd} = $lex;
	}
}
close($FH);

my %wfreq;
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my @wrds = split(/\s+/, $txt);
	foreach my $wrd(@wrds){
		if(exists($wfreq{$wrd})){
			$wfreq{$wrd} += 1;
		}else{
			$wfreq{$wrd} = 1;
		}
	}
}
close($FH);

open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my $outline = $id;
	my @wrds = split(/\s+/, $txt);
	foreach my $wrd (@wrds){
		if($wfreq{$wrd} < $WFTHRESHOLD){	# appears < $WFTHRESHOLD, irrespective of whether in lexicon on not
			$outline .= " <unk>";
		}elsif(($wfreq{$wrd} <= $LEXWFTHRESHOLD) && (!exists($lexicon{$wrd}))){	# not in lexicon and appears <= $LEXWFTHRESHOLD
			$outline .= " <unk>";
		}else{
			$outline .= " $wrd";
		}	
	}
	print $outline."\n";
}
close($FH);